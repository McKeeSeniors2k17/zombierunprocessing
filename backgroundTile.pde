class backgroundTile{
  int tileWidth = 500, tileHeight = 100;
  int xpos=250,ypos, obsPos = -1;
  PImage tile, obs;
  
  backgroundTile(int _ypos, boolean obstacle){
    ypos = _ypos;
    if (obstacle){
      obsPos = ceil(random(0,3));
      obs = loadImage("obs.png");
    }
    tile = loadImage("backgroundTile.png");
  }
  void display(){
    imageMode(CENTER);
    image(tile, xpos, ypos, tileWidth, tileHeight);
    if (obsPos == 1){
      image(obs, 150, ypos);
    }
    if (obsPos == 2){
      image(obs, 250, ypos);
    }
    if (obsPos == 3){
      image(obs, 350, ypos);
    }
  }
  void moveUP(){
   ypos+=5;
  }
}