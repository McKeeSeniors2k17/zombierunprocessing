backgroundTile[] gameGrid = new backgroundTile[500];
int interval = 10, millis=millis(), pos=2,score=0;
PImage dude, zombies; 


void setup(){
  size(500,900);
  dude = loadImage("dude.png");
  zombies = loadImage("zambies.png");
  background(0);
  for(int i = 0; i < gameGrid.length; i++){
    println(i+","+(10-ceil(i/50)));
    if (i%(10-ceil(i/50)) == 0){
      gameGrid[i] = new backgroundTile((850-(100*i)),true);
    } else {
      gameGrid[i] = new backgroundTile((850-(100*i)),false);
    }
  }
}

void draw(){
  background(0);
  for(int i = 0; i < gameGrid.length; i++){
    gameGrid[i].display();
  }
  for(int i = 0; i < gameGrid.length; i++){
    gameGrid[i].moveUP();
    score+=1;
  }
  if (millis+500>=millis()){
    dude = loadImage("dude1.png");
  } else if (millis+1000==millis()){
    dude = loadImage("dude.png");
    millis=millis();
  }
  textSize(25);
  textAlign(LEFT);
  text("Score : " + str(score) + "mm", 25, 25);
   for(int i = 0; i < gameGrid.length; i++){
    if(gameGrid[i].ypos == 750 && gameGrid[i].obsPos == pos){
      noLoop();
      textSize(64);
      textAlign(CENTER, CENTER);
      fill(255);
      text("Game Over", width/2, height/2);
    }
  }
  imageMode(CENTER);
  image(dude,50+(pos*100),750,28,60);
  imageMode(CENTER);
  image(zombies,250,850);
}

void keyPressed(){
    if( keyCode == LEFT){
    pos -= 1;
  }
  else if(keyCode == RIGHT){
    pos += 1;
  }
}